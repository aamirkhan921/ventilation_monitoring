﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VentilationTrack.Models
{
    public class VentilationMonitorModel
    {
        public List<Department> LstDepartments { get; set; }

        public List<VentilationMonitor> LstVentilationMonitor { get; set; }
    }

    public class VentilationMonitor
    {
        public int EquipmentId { get; set; }

        public string EquipmentName { get; set; }

        public int DepartmentId { get; set; }

        public int VentilationDetailsId { get; set; }

        public int Capacity { get; set; }
        public string TotalCapacity { get; set; }

        public List<EquipmentVentilationDetails> LstEquipmentVentilationDetails  { get; set; }
    }
}
