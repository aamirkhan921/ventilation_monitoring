﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VentilationTrack.Models
{
    public class EquipmentVentilationDetails
    {
        public int ID { get; set; }
        public int DepartmentId { get; set; }
        public int EquipmentId { get; set; }

        public int Capacity { get; set; }

        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }

        [ForeignKey("EquipmentId")]
        public Equipment Equipment { get; set; }
    }
}
