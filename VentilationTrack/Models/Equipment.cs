﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VentilationTrack.Models
{
    public class Equipment
    {
        public int ID { get; set; }

        public string Name { get; set; }
        public bool IsActive { get; set; }

        public ICollection<EquipmentVentilationDetails> EquipmentVentilationDetails { get; set; }
    }
}
