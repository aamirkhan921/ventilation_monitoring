﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VentilationTrack.Models
{
    public class VentilationContext : DbContext
    {
        //public VentilationContext()
        //{
        //}

        public VentilationContext(DbContextOptions<VentilationContext> options)
            : base(options)
        {
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        optionsBuilder.UseSqlServer("Server=dbinterview9.database.windows.net;Database=dbInterview;User Id=dbinterview9user;password=Hxes-Ku2Eqc6)x8u;Trusted_Connection=False;MultipleActiveResultSets=true;");
        //    }
        //}

        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Equipment> Equipment { get; set; }
        public virtual DbSet<EquipmentVentilationDetails> EquipmentVentilationDetails { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Department>()
                 .Property(s => s.IsActive)
                 .HasDefaultValue(true);
            modelBuilder.Entity<Department>()
                 .Property(s => s.Name)
                 .IsRequired(true);
            modelBuilder.Entity<Department>()
                 .Property(s => s.VentilationCapacity)
                 .IsRequired(true);
            modelBuilder.Entity<Equipment>()
                .Property(s => s.IsActive)
                 .HasDefaultValue(true);
            modelBuilder.Entity<Equipment>()
                .Property(s => s.Name)
                 .IsRequired(true);
            modelBuilder.Entity<Department>()
                .HasData(
                    new Department
                    {
                        ID=1,
                        Name = "Longwall",
                        VentilationCapacity = 115
                    },
                    new Department
                    {
                        ID = 2,
                        Name = "Tailgate",
                        VentilationCapacity = 70
                    },
                    new Department
                    {
                        ID = 3,
                        Name = "Maingate 13",
                        VentilationCapacity = 49
                    },
                    new Department
                    {
                        ID = 4,
                        Name = "Maingate 14",
                        VentilationCapacity = 52
                    }
                );

            modelBuilder.Entity<Equipment>()
             .HasData(
                 new Equipment
                 {
                     ID = 1,
                     Name = "Loader" 
                 },
                 new Equipment
                 {
                     ID = 2,
                     Name = "Transporter" 
                 } 
             );

            modelBuilder.Entity<EquipmentVentilationDetails>()
             .HasData(
                 new EquipmentVentilationDetails
                 {
                     ID = 1,
                     DepartmentId =1,
                     EquipmentId=1,
                     Capacity=40
                 },
                 new EquipmentVentilationDetails
                 {
                     ID = 2,
                     DepartmentId = 1,
                     EquipmentId = 2,
                     Capacity = 80
                 },
                 new EquipmentVentilationDetails
                 {
                     ID = 3,
                     DepartmentId = 2,
                     EquipmentId = 2,
                     Capacity = 14
                 } ,
                 new EquipmentVentilationDetails
                 {
                     ID = 4,
                     DepartmentId = 3,
                     EquipmentId = 2,
                     Capacity = 20
                 },
                 new EquipmentVentilationDetails
                 {
                     ID = 5,
                     DepartmentId = 4,
                     EquipmentId = 1,
                     Capacity = 15
                 },
                 new EquipmentVentilationDetails
                 {
                     ID = 6,
                     DepartmentId = 4,
                     EquipmentId = 2,
                     Capacity = 25
                 }
             );

        }


    }
}
