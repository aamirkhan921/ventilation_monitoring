﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VentilationTrack.Models
{
    public class Department
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int VentilationCapacity { get; set; }
        public bool IsActive { get; set; }
        public ICollection<EquipmentVentilationDetails> EquipmentVentilationDetails { get; set; }
    }
}
