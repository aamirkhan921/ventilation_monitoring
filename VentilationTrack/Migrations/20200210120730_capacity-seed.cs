﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VentilationTrack.Migrations
{
    public partial class capacityseed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Equipment",
                columns: new[] { "ID", "Name" },
                values: new object[] { 1, "Loader" });

            migrationBuilder.InsertData(
                table: "Equipment",
                columns: new[] { "ID", "Name" },
                values: new object[] { 2, "Transporter" });

            migrationBuilder.InsertData(
                table: "EquipmentVentilationDetails",
                columns: new[] { "ID", "Capacity", "DepartmentId", "EquipmentId" },
                values: new object[,]
                {
                    { 1, 40, 1, 1 },
                    { 5, 15, 4, 1 },
                    { 2, 80, 1, 2 },
                    { 3, 14, 2, 2 },
                    { 4, 20, 3, 2 },
                    { 6, 25, 4, 2 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "EquipmentVentilationDetails",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "EquipmentVentilationDetails",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "EquipmentVentilationDetails",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "EquipmentVentilationDetails",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "EquipmentVentilationDetails",
                keyColumn: "ID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "EquipmentVentilationDetails",
                keyColumn: "ID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Equipment",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Equipment",
                keyColumn: "ID",
                keyValue: 2);
        }
    }
}
