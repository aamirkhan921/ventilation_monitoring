﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VentilationTrack.Models;

namespace VentilationTrack.Controllers
{
    public class VentilationTrackController : Controller
    {
        private readonly VentilationContext _context;
        public VentilationTrackController(VentilationContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            VentilationMonitorModel model = new VentilationMonitorModel();

            model.LstDepartments = _context.Department.Where(p => p.IsActive).ToList();
            var lstEquipment = _context.Equipment.Where(p => p.IsActive).ToList();

            var lstEquipmentVentilationDetails = _context.EquipmentVentilationDetails.Where(p => p.Equipment.IsActive).ToList();

            model.LstVentilationMonitor = new List<VentilationMonitor>();

            if (lstEquipment != null)
            {
                foreach (var equipment in lstEquipment)
                {
                    VentilationMonitor ventilationMonitor = new VentilationMonitor();
                    ventilationMonitor.EquipmentId = equipment.ID;
                    ventilationMonitor.EquipmentName = equipment.Name;

                    ventilationMonitor.LstEquipmentVentilationDetails = new List<EquipmentVentilationDetails>();

                    if (model.LstDepartments != null)
                    {
                        foreach (var department in model.LstDepartments)
                        {
                            EquipmentVentilationDetails equipmentVentilationDetails = new EquipmentVentilationDetails();

                            equipmentVentilationDetails.DepartmentId = department.ID;

                            var ventilationDetails = lstEquipmentVentilationDetails.FirstOrDefault(p => p.DepartmentId == department.ID && p.EquipmentId == equipment.ID);
                            if (ventilationDetails != null)
                            {
                                equipmentVentilationDetails.ID = ventilationDetails.ID;
                                equipmentVentilationDetails.Capacity = ventilationDetails.Capacity;
                            }
                            ventilationMonitor.TotalCapacity = lstEquipmentVentilationDetails.Where(p => p.DepartmentId == department.ID).Sum(p => p.Capacity).ToString();

                            ventilationMonitor.LstEquipmentVentilationDetails.Add(equipmentVentilationDetails);
                        }
                    }

                    model.LstVentilationMonitor.Add(ventilationMonitor);

                }
            }

            return View(model);
        }

        public IActionResult AddNewEqupimentRow()
        {
            VentilationMonitorModel model = new VentilationMonitorModel();

            model.LstDepartments = _context.Department.Where(p => p.IsActive).ToList();

            //Create new equipment with Name 'New Equipment'
            var equipment = new Equipment()
            {
                Name = "New Equipment",
                IsActive = true
            };
            _context.Add(equipment);
            _context.SaveChanges();

            model.LstVentilationMonitor = new List<VentilationMonitor>();

            VentilationMonitor ventilationMonitor = new VentilationMonitor();
            ventilationMonitor.EquipmentId = equipment.ID;//equipment id;
            ventilationMonitor.EquipmentName = equipment.Name;// equipment.Name;

            ventilationMonitor.LstEquipmentVentilationDetails = new List<EquipmentVentilationDetails>();

            if (model.LstDepartments != null)
            {
                foreach (var department in model.LstDepartments)
                {
                    EquipmentVentilationDetails equipmentVentilationDetails = new EquipmentVentilationDetails();
                    equipmentVentilationDetails.DepartmentId = department.ID;

                    ventilationMonitor.LstEquipmentVentilationDetails.Add(equipmentVentilationDetails);
                }
            }

            return PartialView("~/Views/VentilationTrack/_VentilationEquipmentRow.cshtml", ventilationMonitor);
        }

        [HttpPost]
        public IActionResult DeleteEqupimentRow(int equipmentId)
        {
            var equipment = _context.Equipment.FirstOrDefault(o => o.ID == equipmentId);
            equipment.IsActive = false;

            _context.Entry(equipment).State = EntityState.Modified;
            _context.SaveChanges();

            return Json(true);
        }

        [HttpPost]
        public IActionResult SaveUpdateEquipment(int equipmentId, string equipmentName)
        {
            var equipment = _context.Equipment.FirstOrDefault(o => o.ID == equipmentId);
            equipment.Name = equipmentName;

            _context.Entry(equipment).State = EntityState.Modified;
            _context.SaveChanges();

            return Json(true);
        }

        [HttpPost]
        public IActionResult SaveUpdateEquipmentCapacity(int equipmentId, int departmentId, int ventilationId, int capacity)
        {
            EquipmentVentilationDetails equipmentVentilationDetails = new EquipmentVentilationDetails()
            {
                DepartmentId = departmentId,
                EquipmentId = equipmentId,
                Capacity = capacity
            };

            if (ventilationId == 0)
            {
                _context.Add(equipmentVentilationDetails);
            }
            else
            {
                equipmentVentilationDetails = _context.EquipmentVentilationDetails.FirstOrDefault(o => o.ID == ventilationId);
                equipmentVentilationDetails.Capacity = capacity;
                _context.Entry(equipmentVentilationDetails).State = EntityState.Modified;
            }

            _context.SaveChanges();

            return Json(new { status = true, ventilationId = equipmentVentilationDetails.ID });
        }


    }
}